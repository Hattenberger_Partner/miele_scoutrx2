
var logObj = [];


document.body.innerHTML += '<div id="lw"></div>';

var outputWindow = document.getElementById('lw');

outputWindow.style.width = '600px';
outputWindow.style.height = '150px';
outputWindow.style.backgroundColor = 'rgba(0,0,0,0.8)';
outputWindow.style.fontFamily  = 'Courier';
outputWindow.style.letterSpacing  = '1px';
outputWindow.style.fontSize = '12px';
outputWindow.style.color = '#5aff00';
outputWindow.style.position = 'fixed';
outputWindow.style.top = '0';
outputWindow.style.right = '0';
outputWindow.style.margin = '10px';
outputWindow.style.padding = '10px';
outputWindow.style.border = '1px solid #5aff00';
outputWindow.style.boxShadow = '0px 0px 50px 0px rgba(0,0,0,0.75)';
outputWindow.style.overflowY = 'scroll';
outputWindow.scrollTop = outputWindow.scrollHeight;

var log = function (msg) {
	logObj.push([msg, new Date()]);
	if ( logObj.length > 100 ) {
		logObj.shift();
	}
	printOutput();
}

var printOutput = function () {
	outputWindow.innerHTML = ''
	for (var i = 0; i < logObj.length; i++) {
		outputWindow.innerHTML += '<p style="position: relative">' + getH(logObj[i][1]) + ':' + getM(logObj[i][1]) + ':' + getS(logObj[i][1]) + ': ' + logObj[i][0] + '</p>';
	}
	outputWindow.scrollTop = outputWindow.scrollHeight;
}

var getH = function (d) {
	d = d.getHours();
	if (d < 10) {
		d = '0'+d;
	}
	return d;
}
var getM = function (d) {
	d = d.getMinutes();
	if (d < 10) {
		d = '0'+d;
	}
	return d;
}
var getS = function (d) {
	d = d.getSeconds();
	if (d < 10) {
		d = '0'+d;
	}
	return d;
}

outputWindow.style.display = 'none';