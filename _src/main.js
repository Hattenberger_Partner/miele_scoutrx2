/*============================
=            VARS            =
============================*/

var to         = null;
var htl        = new TimelineMax({repeat: -1, repeatDelay: 0.7});
var active_tab = 'home';
var video, videoSource, q3src, q_vid_3, q4src, q_vid_4, q5src, q_vid_5, q6src, accordeonInt; 
var accordeonCurrentPos = null;
var firstUseFeatures  = true;
var videoSuperviser, hotspot_1_invisible, hotspot_2_invisible, hotspot_3_invisible, hotspot_4_invisible, hotspot_5_invisible, ending;
var homeVideo_waitForPause;
var slidesBackgroundTimeout = null;
var backToHomeCnt = 0;
var backToHomeForceReload = 20;
var globalNoInteractionMiniInt = null;
var globalNoInteractionTimeoutTimeMini = 45000;

var globalNoInteractionTimeout = null;
var globalNoInteractionTimeoutTime = 3600000;
var htlF1       = new TimelineMax({repeat: -1, repeatDelay: 1});
/*======================================
=            initialization            =
======================================*/

var init = function () {
	add_listeners();
	set_hotspots();
	setup_hotspot_pulsation();
	setup_video();
	add_draggables();
	setup_homeVideo();
	$('.slides-container').hide();
	$('#app-navigation-container').css({'bottom': '-80px'});
	$('.feature-slide').hide();
	$('#video-overlay').hide();
	$('.q-vid').hide();
	TweenMax.set('.accordion-down', {visibility: 'visible'});
	TweenMax.set('.accordion-up', {visibility: 'hidden'});
	add_draggables();
	setGlobalNoInteraction();
}

function setup_homeVideo(){
	start_homeVideo();
	document.getElementById("videoplayer_home_black").play();
	document.getElementById("videoplayer_home_red").play();
}

function start_homeVideo(){
	document.getElementById("videoplayer_home_black").currentTime = 0;
	document.getElementById("videoplayer_home_red").currentTime = 0;
	document.getElementById("videoplayer_home_black").play();
	document.getElementById("videoplayer_home_red").play();

	clearInterval(videoSuperviser);
	hotspot_1_invisible = hotspot_2_invisible = hotspot_3_invisible = hotspot_4_invisible = hotspot_5_invisible = ending= true;
	TweenMax.set("#hotspot_1, #hotspot_2, #hotspot_3, #hotspot_4, #hotspot_5", {opacity:0});
	videoSuperviser = setInterval(
		function(){
			if(document.getElementById("videoplayer_home_black").currentTime > 2.5 && hotspot_1_invisible){
				hotspot_1_invisible = false;
				TweenMax.to("#hotspot_1", 2, {opacity:1});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 1 && hotspot_2_invisible){
				hotspot_2_invisible = false;
				TweenMax.to("#hotspot_2", 2, {opacity:1});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 0.5 && hotspot_3_invisible){
				hotspot_3_invisible = false;
				TweenMax.to("#hotspot_3", 2, {opacity:1});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 1.5 && hotspot_4_invisible){
				hotspot_4_invisible = false;
				TweenMax.to("#hotspot_4", 2, {opacity:1});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 3.1 && hotspot_5_invisible){
				hotspot_5_invisible = false;
				TweenMax.to("#hotspot_5", 2, {opacity:1});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 30 && ending){
				ending = false;
				TweenMax.to("#hotspot_1, #hotspot_2, #hotspot_3, #hotspot_4, #hotspot_5", 0.3, {opacity:0});
			};
			if(document.getElementById("videoplayer_home_black").currentTime > 31){
				clearInterval(videoSuperviser);
				document.getElementById("videoplayer_home_black").pause();
				document.getElementById("videoplayer_home_red").pause();
				start_homeVideo();
			};
		}, 
	100);
}



function add_listeners () {
	$('.hotspot').on('click', on_hotspot_clicked_handler);
	$('#nav-btn-home').on('click', nav_home);
	$('.button').not($('.nav-btn-language')).on('click', nav_button_click_handler);
	$('#close-button').on('click', close_fs_video)
	$('.play-btn-1').on('click', play_video_feature_1);
	$('.play-btn-2').on('click', play_video_feature_2);
	$('.play-btn-3').on('click', play_video_feature_3);
	$('.play-btn-4').on('click', play_video_feature_4);
	$('.play-btn-5').on('click', play_video_feature_5);
	$('.list-position').on('click', expandSelAccordeon);
	// $('#nav-btn-autoplay').on('click', handleAutoPlayBtn);
	$('.langen').on('click', change_to_en);
}

function add_draggables(){

	
	// var startX_home = 1600;

	var startX_home = 960;
	TweenMax.set(".separator_button",{scale:0.9});
	TweenMax.set("#separator_home",{left:startX_home-35});
	
	var home_draggable = Draggable.create("#separator_home", {
		type:"x", 
		edgeResistance:1, 
		bounds:'#home-container',
		throwProps:true, 
		zIndexBoost:false,
		onDrag: function(e) {
			xOffsetSwipe = this.x + startX_home;
			document.querySelector(".home_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)";
		},
		onPress:function(e) {
			TweenMax.set(".separator_button",{scale:1});
		},
		onRelease:function(e) {
			TweenMax.set(".separator_button",{scale:0.9});
		},
		onThrowUpdate: function(e) {
			xOffsetSwipe = this.x + startX_home;
			document.querySelector(".home_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)"
		}
	});
	
	

	var startX_f1 = 499;
	TweenMax.set(".separator_button_f",{scale:0.9, transformOrigin:'center center'});
	TweenMax.set("#separator_f1",{left:startX_f1-35});

	var f1_draggable = Draggable.create("#separator_f1", {
		type:"x", 
		edgeResistance:1, 
		bounds:'#image-container_f1',
		throwProps:true, 
		zIndexBoost:false,
		onDrag: function(e) {
			xOffsetSwipe = this.x + startX_f1;
			document.querySelector(".f1_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)";
		},
		onPress:function(e) {
			TweenMax.set(".separator_button_f",{scale:1});
		},
		onRelease:function(e) {
			TweenMax.set(".separator_button_f",{scale:0.9});
		},
		onThrowUpdate: function(e) {
			xOffsetSwipe = this.x + startX_f1;
			document.querySelector(".f1_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)"
		}
	});

	var startX_f2 = 499;
	TweenMax.set(".separator_button_f",{scale:0.9, transformOrigin:'center center'});
	TweenMax.set("#separator_f2",{left:startX_f2-35});

	var f2_draggable = Draggable.create("#separator_f2", {
		type:"x", 
		edgeResistance:1, 
		bounds:'#image-container_f2',
		throwProps:true, 
		zIndexBoost:false,
		onDrag: function(e) {
			xOffsetSwipe = this.x + startX_f2;
			document.querySelector(".f2_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)";
		},
		onPress:function(e) {
			TweenMax.set(".separator_button_f",{scale:1});
		},
		onRelease:function(e) {
			TweenMax.set(".separator_button_f",{scale:0.9});
		},
		onThrowUpdate: function(e) {
			xOffsetSwipe = this.x + startX_f2;
			document.querySelector(".f2_mask").style = "clip: rect(0px, " + xOffsetSwipe +"px, 1080px, 0px)"
		}
	});
}


/*==============================
=            hotspots          =
==============================*/

function set_hotspots () {
	$('#hotspot_1').css({left: 820 , top: 650 });
	$('#hotspot_2').css({left: 1230 ,top: 490 });
	$('#hotspot_3').css({left: 1335 , top: 590 });
	$('#hotspot_4').css({left: 1085 ,top: 470 });
	$('#hotspot_5').css({left: 720 , top: 520 });
}

function setup_hotspot_pulsation () {
	htl.add('zero', 0);
	htl.set('.circle-hs-red', {attr: {r: 15}, css:{strokeWidth: 10}}, 'zero');
	htl.set('.circle-hs-white', {attr: {r: 20}, css:{strokeWidth: 10}}, 'zero');
	htl.to('.circle-hs-red', 1.4, {attr: {r: 45}, css: {strokeWidth: 1}, ease: Power1.easeOut}, 'zero');
	htl.to('.circle-hs-red', 0.5, {opacity: 0}, 'zero+=1.0');
	htl.to('.circle-hs-white', 1.4, {attr: {r: 55}, css: {strokeWidth: 0}, ease: SlowMo.ease.config(0.3, 0.6, false), delay: 0.6}, 'zero');
	htl.play();
}

/*==========================================================================
=          Logic change from homescreen to features slides view            =
==========================================================================*/

function on_hotspot_clicked_handler ( evt ) {
	log($(evt.target).attr('id'));
	var px, py;
	switch ($(evt.target).attr('id')) {
		case 'hotspot_1': {
			px = 820;
			py = 650;
			$('.feature-slide-1').show();
			active_tab = 'feature-1';
			addAccordeonFunctionality();
			
			break;
		}
		case 'hotspot_2': {
			px = 1230;
			py = 490;
			$('.feature-slide-2').show();
			active_tab = 'feature-2';
			addAccordeonFunctionality();
			break;
		}
		case 'hotspot_3': {
			px = 1335;
			py = 590;
			$('.feature-slide-3').show();
			active_tab = 'feature-3';
			addAccordeonFunctionality();
			break;
		}
		case 'hotspot_4': {
			px = 1085;
			py = 470;
			$('.feature-slide-4').show();
			active_tab = 'feature-4';
			addAccordeonFunctionality();
			break;
		}
		case 'hotspot_5': {
			px = 720;
			py = 520;
			$('.feature-slide-5').show();
			active_tab = 'feature-5';
			addAccordeonFunctionality();;
			break;
		}
	}

	set_navigation_tab_active();
	homeVideo_waitForPause = setInterval(
		function(){
			document.getElementById("videoplayer_home_black").pause();
			document.getElementById("videoplayer_home_red").pause();
		}, 
	1500);

	log('px: ' + px + ' py: ' + py);
	// set container clip-path to start position
	$('.slides-container').css('-webkit-clip-path', 'circle(2px at ' + (px + 25) + 'px ' + (py + 25) + 'px)' );
	// set white & red circle to start position
	TweenMax.set('#white_circle', {attr:{cx: px+25, cy: py+25, r: 20}, css: {strokeWidth: 1}});
	TweenMax.set('#red_circle', {attr:{cx: px+25, cy: py+25, r: 20}, css: {strokeWidth: 50}});
	// show feature slides contaienr
	$('.slides-container').show();
	// animate container clip-path open
	TweenMax.to('#slides-container', 1.7, {css: {webkitClipPath: 'circle(1950px at ' + (px + 25) + 'px ' + (py + 25) + 'px)'}, ease: SlowMo.ease.config(0.05, 0.6, false)});
	// animate white circle
	TweenMax.to('#white_circle', 1.7, {css: {strokeWidth: 400}, attr: {r: 2000}, ease: SlowMo.ease.config(0.05, 0.6, false)});
	// animate red circle
	TweenMax.to('#red_circle', 1.3, {css: {strokeWidth: 0}, ease: Power2.easeInOut, delay: 0.05 });
	TweenMax.to('#red_circle', 1.7, {attr: {r: 1200}, ease: SlowMo.ease.config(0.05, 0.6, false), delay: 0.05, onComplete: menue, onCompleteParams: [1] } );

}

function menue ( dir ) {
	switch ( dir ) {
		case 0: {
			// Menu out
			TweenMax.to('#app-navigation-container', 0.5, {css: {bottom: -80}, ease: Power2.easeIn });
			break;
		}
		case 1: {
			// Menu in
			TweenMax.to('#app-navigation-container', 0.5, {css: {bottom: 0}, ease: Power2.easeOut });
			break;
		}
	}
}

/*==================================
=            Navigation            =
==================================*/

function nav_button_click_handler ( evt ) {
	log($(evt.target).attr('feature'));
	set_navigation_tab_active($(evt.target).attr('feature'));
	change_slide($(evt.target).attr('feature'));
}

function set_navigation_tab_active ( feature ) {
	var feat = feature;
	if ( feature == undefined ) { feat = active_tab}
	$('.button').removeClass('active');
	$('.button[feature="' + feat + '"]').addClass('active');
}

function change_slide ( feature ) {

	if ( feature != active_tab ) {
		log('active_tab: ' + active_tab + '  feature passed in: ' + feature);
		$('.feature-slide[feature="' + active_tab + '"]').after( $('.feature-slide[feature="' + feature + '"]') );
		$('.feature-slide[feature="' + feature + '"]').fadeIn(500, function () {
			if(feature != active_tab)
				$('.feature-slide[feature="' + active_tab + '"]').hide();
			active_tab = feature;
			addAccordeonFunctionality();
		});
	}
}

function nav_home () {
	$('.slides-container').fadeOut(500, function(){
		$('.feature-slide').hide();
		active_tab = 'home';
	});
	menue(0);
	clearTimeout(homeVideo_waitForPause);
	clearTimeout(globalNoInteractionInt);


	
	accordeonCurrentPos = null;
	setDefaultImagesOnAcc();
	
	backToHomeCnt++;
	if(backToHomeCnt == backToHomeForceReload) {
		location.reload();
	}
	
	// console.log('backToHomeCnt: ' + backToHomeCnt);
	
	
	
	
	document.getElementById("videoplayer_home_black").play();
	document.getElementById("videoplayer_home_red").play();
}

/*=============================
=            Video            =
=============================*/

function setup_video () {
	video = document.getElementById('videoplayer');
	videoSource = document.getElementById('videoSource');
	video.appendChild(videoSource);
}

function play_video_feature_1 () {
	$('#video-overlay').fadeIn(500);
	play_fs_video ('FEAT_3');
}

function play_video_feature_2 () {
	$('#video-overlay').fadeIn(500);
	play_fs_video ('FEAT_2');
}

function play_video_feature_3 () {
	$('#video-overlay').fadeIn(500);
	play_fs_video ('FEAT_3');
}

function play_video_feature_4 () {
	$('#video-overlay').fadeIn(500);
	play_fs_video ('FEAT_3');
}
function play_video_feature_5 () {
	$('#video-overlay').fadeIn(500);
	play_fs_video ('FEAT_3');
}

function play_fs_video ( src ) {
	videoSource.setAttribute('src', '_vid/' + src + '.mp4'); 
	video.load();
	video.play();
	$(video).bind('ended', function(event) {
		close_fs_video();
	});
}

function close_fs_video () {
	video.pause();
	$('#video-overlay').fadeOut(500);
	
	clearTimeout(globalNoInteractionMiniInt);
	clearTimeout(globalNoInteractionInt);
	setGlobalNoInteraction();
}

function change_to_en () {
	console.log('nav');
	location.href = 'index_en.html';
}

/*=============================
=       Accordeons handler    =
=============================*/


function expandSelAccordeon() {
	/* clear Interval and set variable to false */
	// accordeonUserInterAct = true;	
	setDefaultImagesOnAcc();

	TweenMax.set('.list-position .accordion-progress-bar', { height: 0 });
	
	// var wasInteractionForCurrent = false;

	var total = $('div[feature='+active_tab+'] .list-position').length;
	
	$('div[feature='+active_tab+'] .list-position').not(this).each(function() {
		$(this).removeClass('expanded').addClass('collapsed');		

	});

	$(this).toggleClass('collapsed expanded');
	
	clearTimeout(globalNoInteractionMiniInt);
	clearTimeout(globalNoInteractionInt);
	setGlobalNoInteraction();


	if(accordeonCurrentPos != null && accordeonCurrentPos != $(this).index()) {
		TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + accordeonCurrentPos +') .accordion-down', 0.4, {morphSVG: '.accordion-down',ease: Power4.easeInOut});
		TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + accordeonCurrentPos +') .accordion-down', 0.1, {stroke: "#a9a9a8", delay:0});
		
	}
	// if there was interaction, but also autoplayed item it has to be morphed to default state.
	if(accordeonCurrentPos == $(this).index() && $('div[feature='+active_tab+'] .list-position:eq(' + accordeonCurrentPos + ')').hasClass('collapsed')) {
		// console.log('no');
		// if closing this same as it was opened before
		TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + accordeonCurrentPos +') .accordion-down', 0.4, {morphSVG: '.accordion-down',ease: Power4.easeInOut});
		TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + accordeonCurrentPos +') .accordion-down', 0.1, {stroke: "#a9a9a8", delay:0});
		accordeonCurrentPos = null;
		if(active_tab == 'feature_1')
			checkForAdditionalAssets('overall');		
	} else {
		// opening only
		// if(autoPlayCurrentPos != null) {
			// console.log('no2');
			
			// console.log('$(this).index(): ' + $(this).index());
			TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + $(this).index() +') .accordion-down', 0.4, {morphSVG: '.accordion-up', ease: Power4.easeInOut});
			TweenMax.to('div[feature='+active_tab+'] .list-position:eq(' + $(this).index() +') .accordion-down', 0.1, {stroke: "#FF0009", delay:0});
		
			checkForAdditionalAssets($(this).index());
			accordeonCurrentPos = $(this).index();
		// }
	}
	

	
	// if($(this).index()+1 == total)
		// handleLastItemInAccordeon(7000);
	
	
}

function addAccordeonFunctionality() {
	clearTimeout(globalNoInteractionMiniInt);
	clearTimeout(globalNoInteractionInt);
	setGlobalNoInteraction();
	
	if(active_tab == 'feature-1'){
		addHotSpotBlinkingF1();
		console.log("click");
	}
	
	setDefaultImagesOnAcc();
	
	$('div .list-position').removeClass('expanded').addClass('collapsed');
	TweenMax.set('.list-position .accordion-progress-bar', { height: 0 });
	
	TweenMax.to('div .list-position .accordion-down', 1, {morphSVG: '.accordion-down', ease: Power4.easeInOut});
	TweenMax.to('div .list-position .accordion-down', 0.5, {stroke: "#a9a9a8", delay:0});
	
	if(firstUseFeatures) {
		firstUseFeatures = false;
	}
}
function checkForAdditionalAssets(currentAccordeonItem) {
	// console.log(active_tab);
	
	clearTimeout(slidesBackgroundTimeout);
	if(active_tab == 'feature-5') {
		$('.feature-slide-5').css('background-image', 'url(_img/bg_feature_5_' + (currentAccordeonItem+1) + '.jpg)');
	}
	else if(active_tab == 'feature-4') {
		$('.feature-slide-4').css('background-image', 'url(_img/bg_feature_4_' + (currentAccordeonItem+1) + '.jpg)');
	}
	else if(active_tab == 'feature-3') {
		if(currentAccordeonItem+1 == 3) {
			$('.feature-slide-3').css('background-image', 'url(_img/bg_feature_3_' + (currentAccordeonItem+1) + '.jpg)');
			slidesBackgroundTimeout = setTimeout(function() {
				$('.feature-slide-3').css('background-image', 'url(_img/bg_feature_3_' + (currentAccordeonItem+1) + '_1.jpg)');
			}, 3500);
		} else {
			$('.feature-slide-3').css('background-image', 'url(_img/bg_feature_3_' + (currentAccordeonItem+1) + '.jpg)');
		}
	}
	else if(active_tab == 'feature-1') {
		if(currentAccordeonItem+1 == 1) {
			htlF1.play(0);
			$('.feature-1-hs').css('visibility', 'visible');
			$('.feature-2-hs').css('visibility', 'hidden');
			$('.feature-3-hs').css('visibility', 'hidden');
			$('.feature-4-hs').css('visibility', 'hidden');
			
		} else if(currentAccordeonItem+1 == 2) {
			htlF1.play(0);
			$('.feature-2-hs').css('visibility', 'visible');			
			$('.feature-1-hs').css('visibility', 'hidden');
			$('.feature-3-hs').css('visibility', 'hidden');
			$('.feature-4-hs').css('visibility', 'hidden');
		} else if(currentAccordeonItem+1 == 3) {
			htlF1.play(0);
			$('.feature-3-hs').css('visibility', 'visible');			
			$('.feature-1-hs').css('visibility', 'hidden');
			$('.feature-2-hs').css('visibility', 'hidden');
			$('.feature-4-hs').css('visibility', 'hidden');

		} else if(currentAccordeonItem+1 == 4) {
			htlF1.play(0);
			$('.feature-4-hs').css('visibility', 'visible');
			$('.feature-2-hs').css('visibility', 'hidden');
			$('.feature-3-hs').css('visibility', 'hidden');
			$('.feature-1-hs').css('visibility', 'hidden');
			
		} else {
			htlF1.pause(0);
			$('.feature-1-hs').css('visibility', 'hidden');
			$('.feature-2-hs').css('visibility', 'hidden');
			$('.feature-3-hs').css('visibility', 'hidden');
			$('.feature-4-hs').css('visibility', 'hidden');
		}
	}
	else {
		
	}
}

function setDefaultImagesOnAcc() {
	$('.feature-slide-3').css('background-image', 'url(_img/bg_feature_3_1.jpg)');		
	$('.feature-slide-4').css('background-image', 'url(_img/bg_feature_4_1.jpg)');		
	$('.feature-slide-5').css('background-image', 'url(_img/bg_feature_5_1.jpg)');		
	$('.feature-slide-1-2').css('background-image', 'url(_img/bg_featureOverall.jpg)');		
	$('.feature-1-hs').css('visibility', 'hidden');
	$('.feature-2-hs').css('visibility', 'hidden');
	$('.feature-3-hs').css('visibility', 'hidden');
	$('.feature-4-hs').css('visibility', 'hidden');
}
function setGlobalNoInteraction() {
	globalNoInteractionMiniInt = setTimeout(function() {
		nav_home();
	}, globalNoInteractionTimeoutTimeMini);

	globalNoInteractionInt = setTimeout(function() {
		location.reload();
	}, globalNoInteractionTimeoutTime);
}

function addHotSpotBlinkingF1() {
	htlF1.add('zero', 0);
	htlF1.set('.circle-f1-hs-red', {autoAlpha:0, attr: {r: 4}, css:{strokeWidth: 12}}, 'zero');
	htlF1.set('.circle-f1-hs-outer', {autoAlpha:0, attr: {r: 4}, css:{strokeWidth: 12}}, 'zero');
	
	htlF1.to(['.circle-f1-hs-red', '.circle-f1-hs-outer'], 1, { autoAlpha: 1, ease: Power1.easeOut}, 'zero+=0.1');
	
	htlF1.to('.circle-f1-hs-outer', 1.4, {attr: {r: 25}, css: {strokeWidth: 1}, ease: Power1.easeOut}, 'zero+=1.1');
	htlF1.to('.circle-f1-hs-red', 0.5, {autoAlpha: 0}, 'zero+=1.6');
	htlF1.to('.circle-f1-hs-outer', 0.5, {autoAlpha: 0}, 'zero+=1.9');
}